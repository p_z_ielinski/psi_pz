/*
 * Main.java
 *
 * Created on May 24, 2007, 11:20 AM
 *
 * Author: Tomasz Gebarowski
 */

package digitrecognizer;

import kb.KnowledgeBase;
import nn.BackPropagationNN;
import nn.TestSet;
import gui.DigitGUI;

/**
 * Digit Recognizer Neural Network Self Learning Tool
 * Main Class resposnible for application start.
 */
public class Main {
    
    /** Creates a new instance of Main */
    public Main() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DigitGUI().setVisible(true);
            }
        });
    }
    
}
