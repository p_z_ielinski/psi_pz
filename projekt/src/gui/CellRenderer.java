/*
 * CellRenderer.java
 *
 * Created on May 25, 2007, 6:52 PM
 *
 * Author: Tomasz Gebarowski
 */

package gui;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * Class responsible for chaning rendering behavior of JTable
 * It replaces ticks with black boxes ( used for simulating pixel like style ) 

 */
public class CellRenderer extends JLabel implements TableCellRenderer {
        // This method is called each time a cell in a column
        // using this renderer needs to be rendered.
        public Component getTableCellRendererComponent(JTable table, Object value,
                boolean isSelected, boolean hasFocus, int rowIndex, int vColIndex) {
            // 'value' is value contained in the cell located at
            // (rowIndex, vColIndex)
    
            this.setOpaque(true);
            
            if ( value != null ) {
                Boolean val = (Boolean)value;
                if ( val.booleanValue()  ) {
                    this.setBackground(new Color(0,0,0));
                } else {
                    this.setBackground(new Color(255,255,255));
                }
            } else {
                this.setBackground(new Color(255,255,255));
            }
    
            // Since the renderer is a component, return itself
            return this;
        }
    
        // The following methods override the defaults for performance reasons
        public void validate() {}
        public void revalidate() {}
        protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {}
        public void firePropertyChange(String propertyName, boolean oldValue, boolean newValue) {}
}
