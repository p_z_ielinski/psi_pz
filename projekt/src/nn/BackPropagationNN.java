/*
 * BackPropagationNN.java
 *
 * Created on 5 may 2007, 15:47
 *
 * Author: Tomasz Gebarowski
 */

package nn;
import javax.swing.event.*;
import java.util.*;

/**
 * Neural Netoworks Back Propagation Neural Network using Perceptrons
 *
 * Based on Sacha Barber algorithm for back propagation (.NET):
 * http://www.codeproject.com/KB/recipes/NeuralNetwork_1.aspx
 * 
 */
public class BackPropagationNN extends NeuralNetwork implements Runnable {
    
    /* Stores current step - used for progress bar */
    public int currentStep;
    /* Linked List used for storing ChangeListener references */
    private LinkedList listenerList;
    
    /* Total number of steps */
    private int steps;
    
    /* TestSets - used for teaching the network */
    private TestSet ts;
    
    private int nInputs;
    private int nOutputs;
    
    /** Creates a new instance of BackPropagationNN */
    public BackPropagationNN(int nInputs, int nHidden, int nOutputs) {
        super(nInputs, nHidden, nOutputs);
        
        this.nInputs = nInputs;
        this.nOutputs = nOutputs;
        
        listenerList = new LinkedList();
    }
    
    /** Method for adding new train case to the network
     *  @param steps int Number of steps
     *  @param ts TestSet Structure containing all the train cases
     */
    public void addTest(int steps, TestSet ts ) {
        this.steps = steps;
        this.ts = ts;
    }
    
    
    /** Set number of steps to perform while learning
     *  @param steps int Number of desired steps
     */
    public void setSteps( int steps ) {
        this.steps = steps;
    }
    
    /** Add specific case set to the neural network knowledge base.
     *  In order to take this case into consideration the network must be re-trained
     *  @param inputs double[] Input values 
     *  @param outputs double[] Output values ( describing output symbol )
     */
    public void addCase(double[] inputs, double[] outputs ) {
        
        if ( ts == null )
            ts = new TestSet(nInputs, nOutputs);
            
        this.ts.addTestSet( inputs, outputs );
    }
    
    /** Implementation of run from Runnable interface. Executing performTraining that teaches the network
     *  @param steps int Number of steps
     *  @param ts TestSet Structure containing all the train cases
     */     
    public void run() {
        performTraining(steps, ts);
    }
    
   /** Teach the network and set the neurons weights
    *  @param steps int Number of steps
    *  @param ts TestSet Structure containing all the train cases 
    */
    public void performTraining(int steps, TestSet ts) {
            
        for (int i = 0; i < steps; i++)
            {
                for (int j = 0; j < ts.getCount(); j++)
                {
                    //feed forward through network
                    processForward(ts.getInputsAt(j));
                    //do the weight changes (pass back)
                    trainNetwork(ts.getOutputsAt(j));
                    
                }
                fireStateChanged(); currentStep = i;
            
            // Pause thread in order to redraw GUI
            try{Thread.sleep(1);} // make the process last a while
                catch (InterruptedException ex){}
          }
          
        

    }
    
    /** Single teaching iteration using back propagation method 
     *  target double[] Target values ( desired )
     */
    public void trainNetwork(double[] target)
    {
        //get momentum values (delta values from last pass)
        double[] delta_hidden = new double[ numberOfHiddens + 1];
        double[] delta_outputs = new double[ numberOfOutputs];

        // Get the delta value for the output layer
        for (int i = 0; i < numberOfOutputs; i++)
        {
            delta_outputs[i] = outputs[i] * (1.0 - outputs[i]) * (target[i] - outputs[i]);
        }
        // Get the delta value for the hidden layer
        for (int i = 0; i < numberOfHiddens + 1; i++)
        {
            double error = 0.0;
            for (int j = 0; j < numberOfOutputs; j++)
            {
                error += hiddenToOutputWeights[i][j] * delta_outputs[j];
            }
            delta_hidden[i] = hiddens[i] * (1.0 - hiddens[i]) * error;
        }
        // Now update the weights between hidden & output layer
        for (int i = 0; i < numberOfOutputs; i++)
        {
            for (int j = 0; j < numberOfHiddens + 1; j++)
            {
                //use momentum (delta values from last pass),
                //to ensure moved in correct direction
                hiddenToOutputWeights[j][i] += learningRate * delta_outputs[i] * hiddens[j];
            }
        }
        // Now update the weights between input & hidden layer
        for (int i = 0; i < numberOfHiddens; i++)
        {
            for (int j = 0; j < numberOfInputs+1; j++)
            {
                //use momentum (delta values from last pass),
                //to ensure moved in correct direction
                inputToHiddenWeights[j][i] += learningRate * delta_hidden[i] * inputs[j];
            }
        }
    }
    
    /** Sigmoid function used to trigger neuron
     *  @param value double Function argument
     *  @return double Function value
     */
    public double trigger( double value ) {
        return 1.0 / (1.0 + Math.pow(Math.E, -value));
    }
    
    
    /** Method used for adding ChangeListener ( for the purpose of progress bar )
     *  @param l ChangeListener ChangeListener to be added to object
     */
    public void addChangeListener(ChangeListener l ) {
        listenerList.add( l );
    }
    
    /**  Used to fire ChangeEvent every iteration to update Progress Bar
     */
    protected void fireStateChanged() {
        
        ChangeEvent event = new ChangeEvent(this);
        LinkedList listeners = listenerList;
              
        Iterator it = listeners.iterator();
        
        while ( it.hasNext() ) {
                Object o = it.next();            
                ((ChangeListener) o).stateChanged(event);
        }
    }
    
    /** Return number of test cases in knowledge base
     *  @return int Number of test cases
     */ 
    public int getNumberOfTestCases() {
        return this.ts.getCount();
    }

    
}
